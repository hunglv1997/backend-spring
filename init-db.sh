#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
        CREATE USER ubackend;
        CREATE DATABASE dbbackend;
        GRANT ALL PRIVILEGES ON DATABASE dbbackend TO ubackend;
EOSQL
