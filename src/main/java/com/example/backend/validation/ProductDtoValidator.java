package com.example.backend.validation;

import com.example.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductDtoValidator {

    private final ProductRepository productRepository;


    @Autowired
    public ProductDtoValidator(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
}
