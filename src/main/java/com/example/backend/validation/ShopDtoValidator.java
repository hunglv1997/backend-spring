package com.example.backend.validation;

import com.example.backend.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShopDtoValidator {
    private final ShopRepository shopRepository;

    @Autowired
    public ShopDtoValidator(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }
}
