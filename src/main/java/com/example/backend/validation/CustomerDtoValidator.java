package com.example.backend.validation;

import com.example.backend.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerDtoValidator {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerDtoValidator(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
}
