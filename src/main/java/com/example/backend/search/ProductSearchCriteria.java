package com.example.backend.search;

import com.example.backend.comon.search.BaseSearchCriteria;
import lombok.Getter;
import lombok.Setter;

/**
 * The properties need to search
 */
@Getter
@Setter
public class ProductSearchCriteria extends BaseSearchCriteria {
    private String name;
}
