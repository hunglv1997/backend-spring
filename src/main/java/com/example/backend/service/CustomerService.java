package com.example.backend.service;

import com.example.backend.dto.CustomerDto;
import com.example.backend.dto.CustomerDummyDto;

import java.util.List;

public interface CustomerService {
    CustomerDto add(CustomerDto customerDto);

    List<CustomerDto> dummy(CustomerDummyDto dummyDto);

    List<CustomerDto> findAll();

    CustomerDto save(CustomerDto customerDto);
}
