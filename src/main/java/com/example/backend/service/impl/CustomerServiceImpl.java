package com.example.backend.service.impl;

import com.example.backend.dto.CustomerDto;
import com.example.backend.dto.CustomerDummyDto;
import com.example.backend.model.Customer;
import com.example.backend.repository.CustomerRepository;
import com.example.backend.service.CustomerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public CustomerDto add(CustomerDto customerDto) {
        return null;
    }

    @Override
    public List<CustomerDto> dummy(CustomerDummyDto dummyDto) {
        LinkedList<Customer> customers =  new LinkedList<>();
        for (int i = 0; i < dummyDto.getSize(); i++) {
            Customer customer = new Customer();
            customer.setFullName("Customer " + i);
            customer.setDateOfBirth(new Date());
            customer.setEmail("Email " + i);
            customer.setCreatedDate(new Date());
            customers.add(customer);
        }
        return this.customerRepository.saveAll(customers).stream().map(CustomerDto::new).collect(Collectors.toList());
    }

    @Override
    public List<CustomerDto> findAll() {
        return this.customerRepository.findAll().stream().map(CustomerDto::new)
                .collect(Collectors.toList());
    }

    @Override
    public CustomerDto save(CustomerDto customerDto) {
        Customer customer = this.dtoToEntity(customerDto);
        Customer savedCustomer = this.customerRepository.save(customer);
        return new CustomerDto(savedCustomer);
    }

    private Customer dtoToEntity(CustomerDto customerDto) {
        Customer customer = new Customer();
        customer.setCreatedDate(new Date());
        BeanUtils.copyProperties(customerDto, customer);
        return customer;
    }


}
