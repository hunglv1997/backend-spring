package com.example.backend.service.impl;

import com.example.backend.dto.CustomerDto;
import com.example.backend.dto.ShopDummyDto;
import com.example.backend.dto.ShopDto;
import com.example.backend.model.Customer;
import com.example.backend.model.Shop;
import com.example.backend.repository.ShopRepository;
import com.example.backend.service.ShopService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShopServiceImpl implements ShopService {

    private final ShopRepository shopRepository;

    @Autowired
    public ShopServiceImpl(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Override
    public List<ShopDto> dummy(ShopDummyDto dummyDto) {
        LinkedList<Shop> shops =  new LinkedList<>();
        for (int i = 0; i < dummyDto.getSize(); i++) {
            Shop shop = new Shop();
            shop.setLocation("Location " + i );
            shop.setName("Name " + i);
            shop.setCreatedDate(new Date());
            shops.add(shop);
        }
        return this.shopRepository.saveAll(shops).stream().map(ShopDto::new).collect(Collectors.toList());
    }

    @Override
    public ShopDto save(ShopDto shopDto) {
        Shop shop = this.dtoToEntity(shopDto);
        Shop savedShop = this.shopRepository.save(shop);
        return new ShopDto(savedShop);
    }

    @Override
    public List<ShopDto> findAll() {
        return this.shopRepository.findAll().stream().map(ShopDto::new)
                .collect(Collectors.toList());
    }

    private Shop dtoToEntity(ShopDto shopDto) {
        Shop shop = new Shop();
        shop.setCreatedDate(new Date());
        BeanUtils.copyProperties(shopDto, shop);
        return shop;
    }
}
