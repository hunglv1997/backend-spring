package com.example.backend.service.impl;

import com.example.backend.dto.*;
import com.example.backend.service.ProductService;
import com.example.backend.comon.search.BaseSearchCriteria;
import com.example.backend.comon.search.PageSearchResult;
import com.example.backend.model.Customer;
import com.example.backend.model.Product;
import com.example.backend.model.Shop;
import com.example.backend.repository.CustomerRepository;
import com.example.backend.repository.ProductRepository;
import com.example.backend.repository.ShopRepository;
import com.example.backend.search.ProductSearchCriteria;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ShopRepository shopRepository;
    private final CustomerRepository customerRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ShopRepository shopRepository, CustomerRepository customerRepository) {
        this.productRepository = productRepository;
        this.shopRepository = shopRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public List<ProductDto> findAll() {
        return this.productRepository.findAll().stream().map(ProductDto::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductCustomerShopDto> findListProductCustomerShop() {
        return null;
    }

    @Override
    public List<ProductDto> dummy(ProductDummyDto dummyDto) {
        Random rand = new Random();
        LinkedList<Product> products = new LinkedList<>();
        for (int i = 0; i < dummyDto.getSize(); i++) {
            Product product = new Product();
            product.setCreatedDate(new Date());
            product.setName("Name " + i);
            product.setPrice(rand.nextInt(22) / 10.0);
            product.setImage("Image " + i);

            Shop shop = this.shopRepository.findRandomShop();
            product.setShop(shop);

            Customer customer = this.customerRepository.findRandomCustomer();
            product.setCustomer(customer);

            products.add(product);
        }
        return this.productRepository.saveAll(products).stream().map(ProductDto::new).collect(Collectors.toList());
    }

    @Override
    public PageSearchResult<ProductDto> search(ProductSearchCriteria criteria) {
        PageSearchResult<Product> page = this.productRepository.search(criteria);
        List<ProductDto> items =  page.getItems().stream().map(ProductDto::new).collect(Collectors.toList());
        return new PageSearchResult<>(page.getTotalRows(), items);
    }

    @Override
    public PageSearchResult<ProductCustomerShopDto> getListProductCustomerShop(BaseSearchCriteria request) {
        return  this.productRepository.findProductCustomerShop(request.getPageIndex(), request.getPageSize());
    }

    @Override
    public ProductDto save(ProductDto productDto) {
        Product product = this.dtoToEntity(productDto);
        Product savedProduct = this.productRepository.save(product);
        return new ProductDto(savedProduct);
    }

    private Product dtoToEntity(ProductDto productDto) {
        Product product = new Product();
        product.setCreatedDate(new Date());
        BeanUtils.copyProperties(productDto, product);
        return product;
    }
}
