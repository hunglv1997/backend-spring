package com.example.backend.service;


import com.example.backend.comon.search.BaseSearchCriteria;
import com.example.backend.comon.search.PageSearchResult;
import com.example.backend.dto.CustomerDto;
import com.example.backend.dto.ProductCustomerShopDto;
import com.example.backend.dto.ProductDto;
import com.example.backend.dto.ProductDummyDto;
import com.example.backend.search.ProductSearchCriteria;

import java.util.List;

public interface ProductService {

    List<ProductDto> findAll();

    List<ProductCustomerShopDto> findListProductCustomerShop();

    List<ProductDto> dummy(ProductDummyDto dummyDto);

    PageSearchResult<ProductDto> search(ProductSearchCriteria criteria);

    PageSearchResult<ProductCustomerShopDto> getListProductCustomerShop(BaseSearchCriteria request);

    ProductDto save(ProductDto productDto);
}
