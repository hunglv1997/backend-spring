package com.example.backend.service;

import com.example.backend.dto.ShopDummyDto;
import com.example.backend.dto.ShopDto;

import java.util.List;

public interface ShopService {
    List<ShopDto> dummy(ShopDummyDto dummyDto);

    ShopDto save(ShopDto shopDto);

    List<ShopDto> findAll();
}
