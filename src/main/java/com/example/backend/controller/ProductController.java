package com.example.backend.controller;

import com.example.backend.dto.ProductDummyDto;
import com.example.backend.dto.ShopDto;
import com.example.backend.service.ProductService;
import com.example.backend.comon.search.BaseSearchCriteria;
import com.example.backend.comon.search.PageSearchResult;
import com.example.backend.dto.ProductCustomerShopDto;
import com.example.backend.dto.ProductDto;
import com.example.backend.search.ProductSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @PostMapping("/add")
    public ProductDto save(@RequestBody ProductDto productDto) {
        return this.productService.save(productDto);
    }

    @GetMapping("/all")
    public List<ProductDto> getAllProducts() {
        return this.productService.findAll();
    }

    @PostMapping("/dummy")
    public List<ProductDto> dummy(@RequestBody ProductDummyDto productDummyDto) {
        return this.productService.dummy(productDummyDto);
    }
    @GetMapping("/search")
    public PageSearchResult<ProductDto> search(
            @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "query", defaultValue = "") String query
    ) {
        ProductSearchCriteria criteria = new ProductSearchCriteria();
        criteria.setName(query);
        criteria.setOrderAsc(true);
        criteria.setOrderBy("name");
        criteria.setPageIndex(pageIndex);
        criteria.setPageSize(pageSize);
        return this.productService.search(criteria);
    }

    @GetMapping("/list")
    public PageSearchResult<ProductCustomerShopDto> listProduct(
            @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "query", defaultValue = "") String query
    ) {
        BaseSearchCriteria request = new BaseSearchCriteria();
        request.setPageIndex(pageIndex);
        request.setPageSize(pageSize);
        PageSearchResult<ProductCustomerShopDto> page =  this.productService.getListProductCustomerShop(request);
        return  page;
    }

}
