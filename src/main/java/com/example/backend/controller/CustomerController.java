package com.example.backend.controller;

import com.example.backend.dto.CustomerDto;
import com.example.backend.dto.CustomerDummyDto;
import com.example.backend.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("customer")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/add")
    public CustomerDto save(@RequestBody CustomerDto customerDto) {
        return this.customerService.save(customerDto);
    }

    @GetMapping("/all")
    public List<CustomerDto> findAll() {
        return this.customerService.findAll();
    }

    @PostMapping("/dummy")
    public List<CustomerDto> dummy(@RequestBody CustomerDummyDto customerDummyDto) {
        return this.customerService.dummy(customerDummyDto);
    }

}
