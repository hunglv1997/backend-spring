package com.example.backend.controller;

import com.example.backend.dto.CustomerDto;
import com.example.backend.dto.ProductDto;
import com.example.backend.dto.ShopDto;
import com.example.backend.dto.ShopDummyDto;
import com.example.backend.model.Shop;
import com.example.backend.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("shop")
public class ShopController {

    private final ShopService shopService;

    @Autowired
    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping("/all")
    public List<ShopDto> getAll() {
        return this.shopService.findAll();
    }

    @PostMapping("/add")
    public ShopDto save(@RequestBody ShopDto shopDto) {
        return this.shopService.save(shopDto);
    }

    @PostMapping("/dummy")
    public List<ShopDto> dummy(@RequestBody ShopDummyDto shopDummyDto) {
        return this.shopService.dummy(shopDummyDto);
    }

}
