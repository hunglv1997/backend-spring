package com.example.backend.comon.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PageSearchResult<T> {
    private long totalRows;
    private List<T> items;
}
