package com.example.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductCustomerShopDto {
    private String customerFullName;
    private String customerEmail;
    private String shopName;
    private String shopLocation;
    private String productName;
    private String productImage;
    private Double productPrice;
}
