package com.example.backend.dto;


import com.example.backend.model.Shop;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShopDto {
    private int id;
    private String name;
    private String location;

    public ShopDto(Shop shop) {
        BeanUtils.copyProperties(shop, this);
    }
}
