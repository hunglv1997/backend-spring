package com.example.backend.dto;

import com.example.backend.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private int id;
    private int idCustomer;
    private int idShop;
    private String name;
    private double price;
    private String image;

    public ProductDto(Product product) {
        BeanUtils.copyProperties(product, this);
    }

}
