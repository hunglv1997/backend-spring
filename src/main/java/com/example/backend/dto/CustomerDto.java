package com.example.backend.dto;

import com.example.backend.model.Customer;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {
    private int id;
    private String fullName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dateOfBirth;
    private String email;

    public CustomerDto(Customer customer) {
        BeanUtils.copyProperties(customer, this);
    }
}
