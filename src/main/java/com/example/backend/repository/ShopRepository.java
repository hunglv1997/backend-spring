package com.example.backend.repository;

import com.example.backend.model.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopRepository extends JpaRepository<Shop, Integer> {
    @Query(value = "SELECT * FROM shop ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
    Shop findRandomShop();

}
