package com.example.backend.repository;

import com.example.backend.dto.ProductCustomerShopDto;
import com.example.backend.model.Product;
import com.example.backend.comon.search.PageSearchResult;
import com.example.backend.search.ProductSearchCriteria;

public interface ProductRepositoryCustom {
    PageSearchResult<Product> search(ProductSearchCriteria criteria);

    PageSearchResult<ProductCustomerShopDto> findProductCustomerShop(Integer pageIndex, Integer pageSize);
}
