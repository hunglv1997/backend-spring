package com.example.backend.repository.impl;

import com.example.backend.dto.ProductCustomerShopDto;
import com.example.backend.model.Product;
import com.example.backend.model.QCustomer;
import com.example.backend.model.QProduct;
import com.example.backend.model.QShop;
import com.example.backend.comon.search.BaseSearchCriteria;
import com.example.backend.comon.search.PageSearchResult;
import com.example.backend.comon.search.QueryExecutor;
import com.example.backend.repository.ProductRepositoryCustom;
import com.example.backend.search.ProductSearchCriteria;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.jpa.JPQLTemplates;
import com.querydsl.jpa.impl.JPAQuery;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import com.querydsl.jpa.impl.JPAQueryFactory;

public class ProductRepositoryCustomImpl extends QueryExecutor implements ProductRepositoryCustom {

    private static final QProduct qProduct = QProduct.product;
    private static final QCustomer qCustomer = QCustomer.customer;
    private static final QShop qShop = QShop.shop;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PageSearchResult<Product> search(ProductSearchCriteria criteria) {
        JPAQuery<Product> query = new JPAQuery<Product>(entityManager)
                .from(qProduct)
                .where(predicateOf(criteria));
        return super.executeQuery(criteria, query);
    }

    @Override
    public PageSearchResult<ProductCustomerShopDto> findProductCustomerShop(Integer pageIndex, Integer pageSize) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(JPQLTemplates.DEFAULT, entityManager);

        QBean<ProductCustomerShopDto> qBean = Projections.bean(ProductCustomerShopDto.class,
                qCustomer.fullName.as("customerFullName"),
                qCustomer.email.as("customerEmail"),
                qShop.name.as("shopName"),
                qShop.location.as("shopLocation"),
                qProduct.name.as("productName"),
                qProduct.image.as("productImage"),
                qProduct.price.as("productPrice")
        );

        JPAQuery<ProductCustomerShopDto> query =  queryFactory.select(qBean)
                .from(qProduct)
                .innerJoin(qProduct.customer, qCustomer)
                .innerJoin(qProduct.shop, qShop)
                .orderBy(qCustomer.email.asc(), qShop.location.desc(), qProduct.price.desc())
                ;

        BaseSearchCriteria criteria = new BaseSearchCriteria();
        criteria.setPageIndex(pageIndex);
        criteria.setPageSize(pageSize);
        PageSearchResult<ProductCustomerShopDto>  result =  super.executeQuery(criteria, query);
        return result;
    }

    private BooleanBuilder predicateOf(ProductSearchCriteria criteria) {
        BooleanBuilder predicate =  new BooleanBuilder();

        String name = criteria.getName();
        if(StringUtils.isNotBlank(name))
            predicate.and(qProduct.name.containsIgnoreCase(name));

        return predicate;
    }




}
